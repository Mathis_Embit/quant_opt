#pragma once
#pragma warning(disable : 4996)
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
using namespace cv;
using namespace std;

class DiscreteDistrib; // pour eviter une boucle d'include en image.h et discreteDistrib.h

class Image {
private:
	Mat image; // Mat est une classe provenant d'opencv pour manipuler les images
	int** matrix; // on utilise aussi une matrice qui correspond a image car plus simple pour creer la distrib discrete (mais surement possible de s'en separer)
	int width;
	int height;
public:
	Image() {}

	Image(string filename);

	Image(int width, int height);

	Image(int** M, int width, int height);

	Image(Mat img);

	~Image();

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	Mat getImage() {
		return image;
	}

	int** getMatrix() {
		return matrix;
	}

	int getPixel(int i, int j) {
		return matrix[i][j];
	}

	void setPixel(int i, int j, int value) {
		matrix[i][j] = value; // on met a jour notre matrice
		image.at<uchar>(i, j) = value; // on met a jour l'image opencv
	}

	void imageDistribTest(int nbPoints, string exportFilename);

	Ensemble quantification(int N, int nbRandPoints, int nbIter, string exportFilename);

	void compress(Ensemble* lesRepresentants, string exportFilename);

	void autreMethode(int N, int nbRandPoints, int nbIter, string exportFilename);

};
