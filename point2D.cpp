#include "ensemble.h"

bool Point2D::isInCell(Point2D* representant, Ensemble* lesRepresentants) {
	// retourne vrai ssi le point point est dans la cellule de Voronoi du point representant
	// point et representant sont des vecteurs de R^2
	// lesRepresentants est un ensemble qui contient des Point2D
	double d = distanceTo(representant); // on va comparer d a toutes les autres distances

	for (int i = 0; i < lesRepresentants->getSize(); i++) {
		// on parcourt toutes les cellules grace aux representants
		Point2D* rep = lesRepresentants->getPoint(i);
		if (distanceTo(rep) < d) // si on en trouve un qui est plus proche de point
			return false; // alors point n'est pas dans la cellule
	}
	return true; // puisqu'on a rien trouve de plus proche on retourne TRUE
}

Point2D* Point2D::getClosest(Ensemble* lesPoints) {
	// retourne le point le proche du point implicite parmi lesPoints
	double d = distanceTo(lesPoints->getPoint(0));
	int closest = 0;
	for (int i = 0; i < lesPoints->getSize(); i++) {
		Point2D* p = lesPoints->getPoint(i);
		if (distanceTo(p) < d) {
			closest = i;
			d = distanceTo(lesPoints->getPoint(i));
		}
	}
	return lesPoints->getPoint(closest);
}
