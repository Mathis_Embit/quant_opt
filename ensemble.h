#pragma once
#pragma warning(disable : 4996)
#include <string>
#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include "point2D.h"
using namespace cv;
using namespace std;

class Ensemble {
private:
	vector<Point2D*> _lesPoints; // un ensemble est une collection (vector) de points
public:
	Ensemble() {}

	void copie(const Ensemble& o);

	void copie(Ensemble* o);

	Ensemble(const Ensemble& o);

	Ensemble(Ensemble* o);

	void destructeur();

	~Ensemble();

	Point2D* getPoint(int i) const {
		return _lesPoints.at(i);
	}

	vector<Point2D*> getLesPoint() const {
		return _lesPoints;
	}

	const Point2D* operator [](int i)const {
		return getPoint(i);
	}

	const int getSize() {
		return (int)_lesPoints.size();
	}

	void addPoint(Point2D* p);

	void delPoint(int i);

	void delPoint(Point2D* comp);

	void viewEnsemble();

	void clear();

	void printImage(int width, int height, string exportFilename);

	Ensemble pointsInCellOf(Point2D* representant, Ensemble* lesRepresentants);

	Point2D moyEmp();

};
