#pragma once
#include <stdlib.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <random>
#include <string>

using namespace std;

class Distrib {
// classe parent des distributions en 2D (sur [0,width]x[0,height])
private:
	int width;
	int height;
public:
	Distrib() {}

	Distrib(int width, int height) {
		this->width = width;
		this->height = height;
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}
};
