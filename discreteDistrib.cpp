#include "discreteDistrib.h"

DiscreteDistrib::DiscreteDistrib(int width, int height, int** matrix) :
	Distrib(width, height)
{
	// construit une distrib discrete a partir d'une matrice
	Image weight(matrix, width, height);
	std::vector<int> d(width * height);
	int cpt = 0;
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			d[cpt] = weight.getPixel(i, j);
			cpt++;
		}
	}
	distribution = discrete_distribution<int>(d.begin(), d.end());
}

DiscreteDistrib::DiscreteDistrib(Mat img) :
	Distrib(img.cols, img.rows)
{
	// construit une distrib discrete a partir d'une image opencv
	Image weight(img);

	std::vector<int> d(weight.getWidth() * weight.getHeight());
	int cpt = 0;
	for (int i = 0; i < weight.getHeight(); i++) {
		for (int j = 0; j < weight.getWidth(); j++) {
			d[cpt] = weight.getPixel(i, j);
			cpt++;
		}
	}
	distribution = discrete_distribution<int>(d.begin(), d.end());
}

Point2D DiscreteDistrib::generatePoint() {
	// retourne un point de R^2 aleatoire suivant la distribution
	int number = distribution(generator);
	int x = number / getWidth();
	int y = number % getWidth();
	Point2D res(x, y);
	return res;
}
