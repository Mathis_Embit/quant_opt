#pragma once
#include <stdlib.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <random>
#include <string>
#include "point2D.h"
#include "distrib.h"
#include "image.h"
using namespace std;

class DiscreteDistrib : public Distrib {
private:
	Image weight; // on fait correspondre les poids de la distrib discrete en 2D a notre classe Image
	default_random_engine generator;
	discrete_distribution<int> distribution;
public:
	DiscreteDistrib(int width, int height, int** matrix);

	DiscreteDistrib(Mat img);

	int probaOf(int x, int y) {
		return weight.getPixel(x, y);
	}

	int probaOf(Point2D* p) {
		return weight.getPixel((int)(p->getX()), (int)(p->getY()));
	}

	Point2D generatePoint();

};
