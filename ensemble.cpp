#include "ensemble.h"

void Ensemble::copie(const Ensemble& o) {
	for (int i = 0; i < o._lesPoints.size(); i++) { // pour tous lesPoints de l'ensemble a copier
		_lesPoints.push_back(o._lesPoints[i]->clone());
	}
}

void Ensemble::copie(Ensemble* o) {
	for (int i = 0; i < o->getSize(); i++) { // pour tous lesPoints de l'ensemble a copier
		_lesPoints.push_back(o->getPoint(i)->clone());
	}
}

Ensemble::Ensemble(const Ensemble& o) {
	copie(o);
}

Ensemble::Ensemble(Ensemble* o) {
	copie(o);
}

void Ensemble::destructeur() {
	_lesPoints.clear();
}

Ensemble::~Ensemble() {
	destructeur();
}

void Ensemble::addPoint(Point2D* p) {
	_lesPoints.push_back(p->clone());
}

void Ensemble::delPoint(int i) {
	if (i < 0 || i >= _lesPoints.size()) throw exception();
	delete _lesPoints[i];
	_lesPoints.erase(_lesPoints.begin() + i);
}

void Ensemble::delPoint(Point2D* comp) {
	for (int i = 0; i < _lesPoints.size(); i++) {
		if (_lesPoints[i] == comp)
			_lesPoints.erase(_lesPoints.begin() + i);
	}
	delete comp;
}

void Ensemble::viewEnsemble() {
	for (int i = 0; i < _lesPoints.size(); i++) {
		_lesPoints[i]->viewPoint2D();
	}
}

void Ensemble::clear() {
	_lesPoints.clear();
}

void Ensemble::printImage(int width, int height, string exportFilename) {
	// on affiche les points de l'ensemble en noir sur une image blanche
	
	// on commence par creer une image blanche
	int** M = (int**)malloc(height * sizeof(int*));
	for (int i = 0; i < height; i++) {
		M[i] = (int*)malloc(width * sizeof(int));
		for (int j = 0; j < width; j++)
			M[i][j] = 255;
	}

	// pour tous les points de l'ensmble, s'il est bien dans le cadre de l'image, le pixel correspondant a ses coordonnees devient noir
	for (int i = 0; i < getSize(); i++) {
		if (getPoint(i)->getX() >= 0 && getPoint(i)->getX() < height && getPoint(i)->getY() >= 0 && getPoint(i)->getY() < width) // 0 <= x < height et 0 <= y < width
			M[(int)(getPoint(i)->getX())][(int)(getPoint(i)->getY())] = 0; // attention : il faut bien faire M[x][y]
	}

	// on transforme la matrice en image opencv
	Mat myImg(height, width, CV_8UC1); // attention : (int rows, int cols, int type)

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			myImg.at<uchar>(i, j) = M[i][j];
		}
	}
	imshow("Mon image", myImg); // affiche l'image dans une fenetre
	imwrite(exportFilename, myImg); // exporte l'image
}

Ensemble Ensemble::pointsInCellOf(Point2D* representant, Ensemble* lesRepresentants) {
	// retourne l'ensemble des points de l'ensemble qui sont dans la cellule du point representant
	Ensemble cellule;
	for (int i = 0; i < getSize(); i++) { // on parcourt tous les points de l'ensemble implicite
		Point2D* p = getPoint(i);
		if (p->isInCell(representant, lesRepresentants)) // on regarde s'il appartient a la cellule de representant
			cellule.addPoint(p); // on ajoute p a l'ensemble des points de la cellule de representant
	}
	return cellule; // on retourne la celulle qui est donc remplie des points plus proche de representant que de n'importe quel autre point dans lesRepresentants
}

Point2D Ensemble::moyEmp() {
	// retourne la moyenne empirique des points que contient l'ensemble
	double sumx = 0;
	double sumy = 0;
	int len = getSize();
	for (int i = 0; i < len; i++) {
		Point2D* p = getPoint(i);
		sumx += p->getX();
		sumy += p->getY();
	}
	double x = sumx / (double)len;
	double y = sumy / (double)len;
	Point2D res(x, y);
	return res;
}
