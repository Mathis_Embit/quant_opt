#pragma once
#pragma warning(disable : 4996)
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include "ensemble.h"
#include "discreteDistrib.h"
#include "point2D.h"
using namespace cv;
using namespace std;

Image::Image(string filename) { // creer une image a partir d'un fichier et creer la matrice correspondante
    image = imread(filename, IMREAD_GRAYSCALE);
    if (image.empty()) { // Error Handling
        cout << "Image File " << "Not Found" << endl;
        cin.get(); // wait for any key press
        exit(-1);
    }
    width = image.cols;
    height = image.rows;
    matrix = (int**)malloc(height * sizeof(int*));
    for (int i = 0; i < height; i++) {
        matrix[i] = (int*)malloc(width * sizeof(int));
        for (int j = 0; j < width; j++) {
            matrix[i][j] = image.at<uchar>(i, j);
        }
    }
}

Image::Image(int width, int height) { // creer une matrice et une image a 0
    this->width = width;
    this->height = height;
    image = Mat(height, width, CV_8UC1); // (int rows, int cols, int type)
    matrix = (int**)malloc(height * sizeof(int*));
    for (int i = 0; i < height; i++) {
        matrix[i] = (int*)malloc(width * sizeof(int));
        for (int j = 0; j < width; j++) {
            matrix[i][j] = 0;
            image.at<uchar>(i, j) = 0;
        }
    }
}

Image::Image(int** M, int width, int height) { // copie la matrice et creer l'image correspondante
    this->width = width;
    this->height = height;
    image = Mat(height, width, CV_8UC1); // (int rows, int cols, int type)
    matrix = (int**)malloc(height * sizeof(int*));
    for (int i = 0; i < height; i++) {
        matrix[i] = (int*)malloc(width * sizeof(int));
        for (int j = 0; j < width; j++) {
            matrix[i][j] = M[i][j];
            image.at<uchar>(i, j) = M[i][j];
        }
    }
}

Image::Image(Mat img) { // copie l'image et construit la matrice correspondante
    image = img;
    width = image.cols;
    height = image.rows;
    matrix = (int**)malloc(height * sizeof(int*));
    for (int i = 0; i < height; i++) {
        matrix[i] = (int*)malloc(width * sizeof(int));
        for (int j = 0; j < width; j++) {
            matrix[i][j] = image.at<uchar>(i, j);
        }
    }
}

Image::~Image() {
    for (int i = 0; i < height; i++) {
        free(matrix[i]);
    }
    free(matrix);
    image.release();
}

Ensemble Image::quantification(int N, int nbRandPoints, int nbIter, string exportFilename) {
    // applique l'algo de Lloyd a la loi discrete de l'image
    // N = nb de points du support
    // nbRandPoints = nb de points utilises pour calculer les esperances conditionnelles des cellules
    // nbIter = nb d'iteration de l'algo de Lloyd
    // exportFilename = nom du fichier exporte

    // complexite = nbIter x nbRandPoint x N

    printf("Quantification en cours...\n");

    int** M = (int**)malloc(height * sizeof(int*));
    for (int i = 0; i < height; i++) {
        M[i] = (int*)malloc(width * sizeof(int));
        for (int j = 0; j < width; j++) {
            M[i][j] = 255 - (int)image.at<uchar>(i, j); // pour que les pixels noirs (= 0) aient le plus grand poids (= 255) et inversement pour les blancs
        }
    }

    DiscreteDistrib distr(width, height, M); // la loi qu'on veut quantifier

    Ensemble lesRepresentants; // c'est le point de depart de l'algo de Lloyd
    for (int i = 0; i < N; i++) { // on en genere N
        Point2D p;
        p = distr.generatePoint();
        lesRepresentants.addPoint(&p);
    }

    Point2D** lesMoyennes = (Point2D**)malloc(lesRepresentants.getSize() * sizeof(Point2D*));
    Ensemble** lesCellules = (Ensemble**)malloc(lesRepresentants.getSize() * sizeof(Ensemble*));

    for (int i = 0; i < nbIter; i++) {
        Ensemble lesPoints;

        for (int l = 0; l < lesRepresentants.getSize(); l++) { // on parcourt tous les representants
            // on creer une cellule et une moyenne pour chaque representant
            lesCellules[l] = new Ensemble();
            lesMoyennes[l] = lesRepresentants.getPoint(l);
        }

        for (int k = 0; k < nbRandPoints; k++) {
            Point2D p;
            p = distr.generatePoint();
            lesPoints.addPoint(&p);

            double d = p.distanceTo(lesRepresentants.getPoint(0));
            int closest = 0;

            for (int l = 0; l < lesRepresentants.getSize(); l++) { // on parcourt tous les representants
                // on regarde de quel representant le point p est le plus proche, cad a quelle cellule il appartient
                if (p.distanceTo(lesRepresentants.getPoint(l)) < d) {
                    closest = l;
                    d = p.distanceTo(lesRepresentants.getPoint(l));
                }
            }

            // on sait que le point p est dans la cellule du representant closest
            lesCellules[closest]->addPoint(&p);
            // on met a jour la moyenne empirique
            double x = ((lesMoyennes[closest]->getX() * lesCellules[closest]->getSize()) + p.getX()) / (lesCellules[closest]->getSize() + 1);
            double y = ((lesMoyennes[closest]->getY() * lesCellules[closest]->getSize()) + p.getY()) / (lesCellules[closest]->getSize() + 1);
            lesMoyennes[closest]->setX(x);
            lesMoyennes[closest]->setY(y);
        }
        // ici on a bien rempli toutes les cellules
        // on met a jour les representants grace aux moyennes empiriques obtenues
        int size = lesRepresentants.getSize();
        lesRepresentants.clear();
        for (int l = 0; l < size; l++) {
            lesRepresentants.addPoint(lesMoyennes[l]);
        }

        printf("Iteration %d / %d\n", i + 1, nbIter);
    }

    lesRepresentants.printImage(width, height, exportFilename);
    return lesRepresentants;
}

void Image::compress(Ensemble* lesRepresentants, string exportFilename) {
    // on parcourt tous les pixels de l'image et on remplace sa couleur par celle de la cellule a laquelle il appartient
    printf("Compression en cours...\n");
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            Point2D* p = new Point2D(i, j);
            Point2D* rep = p->getClosest(lesRepresentants);
            int x = int(rep->getX());
            int y = int(rep->getY());
            this->setPixel(i, j, this->getPixel(x, y)); // le point prend la meme couleur que son representant
        }
        printf("Ligne %d / %d traitee\n", i + 1, height);
    }
    imshow("Mon image", image);
    imwrite(exportFilename, image);
}

void Image::autreMethode(int N, int nbRandPoints, int nbIter, string exportFilename) {
    // astuce pour gagner du temps en evitant la methode compress ci dessus
    // a la derniere iteration, la couleur des points aleatoires utilises pour les esp conditionnelles est remplacee
    // par celle de la cellule dans laquelle ils sont tombes

    printf("Quantification en cours...\n");

    int** M = (int**)malloc(height * sizeof(int*));
    for (int i = 0; i < height; i++) {
        M[i] = (int*)malloc(width * sizeof(int));
        for (int j = 0; j < width; j++) {
            M[i][j] = 255 - (int)image.at<uchar>(i, j); // pour que les pixels noirs (= 0) aient le plus grand poids (= 255) et inversement pour les blancs
        }
    }

    DiscreteDistrib distr(width, height, M); // la loi qu'on veut quantifier

    Ensemble lesRepresentants; // c'est le point de depart de l'algo de Lloyd
    for (int i = 0; i < N; i++) { // on en genere N
        Point2D p;
        p = distr.generatePoint();
        lesRepresentants.addPoint(&p);
    }

    Point2D** lesMoyennes = (Point2D**)malloc(lesRepresentants.getSize() * sizeof(Point2D*));
    Ensemble** lesCellules = (Ensemble**)malloc(lesRepresentants.getSize() * sizeof(Ensemble*));
    for (int i = 0; i < nbIter; i++) {
        Ensemble lesPoints;

        for (int l = 0; l < lesRepresentants.getSize(); l++) { // on parcourt tous les representants
            // on creer une cellule pour chaque representant
            lesCellules[l] = new Ensemble();
            lesMoyennes[l] = lesRepresentants.getPoint(l);
        }

        for (int k = 0; k < nbRandPoints; k++) {
            Point2D p;
            p = distr.generatePoint();
            lesPoints.addPoint(&p);

            double d = p.distanceTo(lesRepresentants.getPoint(0));
            int closest = 0;

            for (int l = 0; l < lesRepresentants.getSize(); l++) { // on parcourt tous les representants
                if (p.distanceTo(lesRepresentants.getPoint(l)) < d) {
                    closest = l;
                    d = p.distanceTo(lesRepresentants.getPoint(l));
                }
            }

            if (i == nbIter - 1) { // SI CEST LA DERNIERE ITERATION
                this->setPixel(int(p.getX()), int(p.getY()), this->getPixel(int(lesRepresentants.getPoint(closest)->getX()), int(lesRepresentants.getPoint(closest)->getY())));
            }

            // on sait que le point p est dans la cellule du representant closest
            lesCellules[closest]->addPoint(&p);
            // on met a jour la moyenne empirique
            double x = ((lesMoyennes[closest]->getX() * lesCellules[closest]->getSize()) + p.getX()) / (lesCellules[closest]->getSize() + 1);
            double y = ((lesMoyennes[closest]->getY() * lesCellules[closest]->getSize()) + p.getY()) / (lesCellules[closest]->getSize() + 1);
            lesMoyennes[closest]->setX(x);
            lesMoyennes[closest]->setY(y);
        }
        // ici on a bien rempli toutes les cellules
        // on met a jour les representants grace aux moyennes empiriques obtenues
        int size = lesRepresentants.getSize();
        lesRepresentants.clear();
        for (int l = 0; l < size; l++) {
            lesRepresentants.addPoint(lesMoyennes[l]);
        }

        printf("Iteration %d / %d\n", i + 1, nbIter);
    }
    lesRepresentants.printImage(width, height, exportFilename);

    imshow("Mon image", image);
    imwrite(exportFilename, image);
}
