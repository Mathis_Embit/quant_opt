#pragma once
#include <iostream>
#include <string>
using namespace std;

class Ensemble; // pour eviter la boucle d'include

class Point2D {
private:
	double _x;
	double _y;
public:
	Point2D() {
		_x = 0;
		_y = 0;
	}

	Point2D(double x, double y) {
		_x = x;
		_y = y;
	}

	Point2D(int x, int y) {
		_x = x;
		_y = y;
	}

	Point2D(Point2D& o) {
		_x = o._x;
		_y = o._y;
	}

	Point2D(Point2D* o) {
		_x = o->getX();
		_y = o->getY();
	}

	double getX() {
		return _x;
	}

	double getY() {
		return _y;
	}

	void setX(double x) {
		_x = x;
	}

	void setY(double y) {
		_y = y;
	}

	void viewPoint2D() {
		printf("(%lf, %lf)\n", _x, _y);
	}

	Point2D* clone() {
		return new Point2D(*this);
	}

	~Point2D() {}


	double distanceTo(Point2D* p) {
		// retourne la distance euclidienne entre le point et un autre point p
		return sqrt(pow(_x - p->_x, 2) + pow(_y - p->_y, 2));
	}

	bool isInCell(Point2D* representant, Ensemble* lesRepresentants);

	Point2D* getClosest(Ensemble* lesPoints);

};
