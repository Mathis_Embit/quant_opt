#pragma once
#include <iostream>
#include <opencv2/opencv.hpp>
#include <random>
#include <string>
#include <ctime>
#include "ensemble.h"
#include "image.h"
using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
    printf("-----------------START-----------------\n");
    std::string srcFolder("images/source/");
    std::string expFolder("images/export/");

    std::string name("taj"); // ex : taj
    std::string filename(name+".jpg"); // ex : taj.jpg

    int N = 1000;
    int nbRandPoints = 100;
    int nbIter = 10;

	Image* img = new Image(srcFolder + filename);
    int t1 = (int)time(NULL);
	Ensemble lesRepresentants(img->quantification(N, nbRandPoints, nbIter, expFolder + "quantified_" + name + "_" + to_string(N) + ".jpg"));
    int t2 = (int)time(NULL);
	img->compress(&lesRepresentants, expFolder + "compressed_" + name + "_" + to_string(N) + ".jpg");
    int t3 = (int)time(NULL);
    printf("\nTemps ecoule quantification = % d\n", t2 - t1);
    printf("Temps ecoule compression    = %d\n", t3 - t2);
    
    // Autre astuce pour compresser
    //Image* img2 = new Image(srcFolder + filename);
    //img2->autreMethode(N, nbRandPoints, nbIter, expFolder + name + "_autre_methode_export.jpg");

    printf("------------------END------------------\n");

    waitKey(0);
    return 0;
}
